﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Admin : User
    {
        public Admin() { }

        public Admin(string[] parts) : base(parts) { }
    }
}