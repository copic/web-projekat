﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class RegisteredUsers
    {
        public static List<Admin> Admins = new List<Admin>();
        public static List<Seller> Sellers = new List<Seller>();
        public static List<Buyer> Buyers = new List<Buyer>();
        public static List<User> AllUsers = new List<User>();

        /// <summary>
        /// Updating all buyers and sellers in Users.txt
        /// </summary>
        public static void UpdateUsers()
        {
            using (var writer = new StreamWriter(Constants.DATA_ROOT_PATH + "Users.txt", false))
            {
                foreach(var u in AllUsers)
                {
                    if (u.Role == Role.Buyer)
                        writer.WriteLine(((Buyer)u).ToString());
                    if (u.Role == Role.Seller)
                        writer.WriteLine(((Seller)u).ToString());
                }                                   
            }
        }

        /// <summary>
        /// Updating all admins in Admins.txt
        /// </summary>
        public static void UpdateAdmins()
        {
            using (var writer = new StreamWriter(Constants.DATA_ROOT_PATH + "Admins.txt", false))
            {
                foreach (var u in AllUsers)
                    if (u.Role == Role.Admin)
                        writer.WriteLine(u.ToString());
            }
        }

        public static Buyer GetBuyer(string username)
        {
            foreach(Buyer b in Buyers)
            {
                if (b.Username == username)
                    return b;
            }

            return null;
        }

        public static Seller GetSeller(string username)
        {
            foreach(Seller s in Sellers)
            {
                if (s.Username == username)
                    return s;
            }

            return null;
        }
    }
}