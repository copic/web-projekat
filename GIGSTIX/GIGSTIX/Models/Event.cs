﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Event
    {
        public string Name { get; set; }
        public string EventType { get; set; }
        public int AvailableTickets { get; set; }
        public DateTime Date { get; set; }
        public float Price { get; set; }
        public bool Status { get; set; }        
        public string Image { get; set; }
        public Location Location { get; set; }
        public List<Comment> Comments { get; set; } = new List<Comment>();
        public float AverageRating { get; set; } = 0;
        private string statusString;
        public bool IsDeleted { get; set; } = false;
        public string Seller { get; set; }

        public Event() { }

        public void RetrieveComments()
        {
            foreach(Comment c in AllTickets.Comments)
            {
                if (c.Event.Name == this.Name)
                    Comments.Add(c);
            }

            CalculateAverageRating();
        }

        public void CalculateAverageRating()
        {
            if (Comments.Any())
            {
                int rating = 0;

                foreach (Comment c in Comments)
                    rating += (int)c.Rating;

                AverageRating = rating / Comments.Count();
            }
        }

        public string GetStatusString()
        {
            if(Status == true)
            {
                SetStatusString("Active");
                return statusString;
            }
            else
            {
                SetStatusString("Inactive");
                return statusString;
            }
        }

        public void SetStatusString(string value)
        {
            statusString = value;
        }

        public bool IsName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return true;

            if (Name == name)
                return true;

            return false;
        }

        public bool IsLocation(string location)
        {
            if (string.IsNullOrEmpty(location))
                return true;

            if (Location.Venue.City == location)
                return true;

            return false;
        }

        public bool IsDate(string date)
        {
            if (string.IsNullOrEmpty(date))
                return true;

            if (Date.ToShortDateString() == date)
                return true;

            return false;
        }

        public bool IsPrice(string priceMin, string priceMax)
        {
            if (string.IsNullOrEmpty(priceMin) && string.IsNullOrEmpty(priceMax))
                return true;

            var intTo = string.IsNullOrEmpty(priceMax) ? int.MaxValue : int.Parse(priceMax);
            var intFrom = string.IsNullOrEmpty(priceMin) ? 0 : int.Parse(priceMin);

            return Price >= intFrom && Price <= intTo;
        }

        public bool IsType(string type)
        {
            if (string.IsNullOrEmpty(type))
                return true;

            if (EventType == type)
                return true;

            return false;
        }

        public bool IsUnsold(string unsold)
        {
            if (string.IsNullOrEmpty(unsold))
                return true;

            if (unsold == "unsold")
                if (AvailableTickets > 0)
                    return true;

            if (unsold == "all")
                return true;

            return false;

        }
    }
}