﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Venue
    {
        public string Street { get; set; }
        public string Number { get; set; }
        public string City { get; set; }
        public string PostNumber { get; set; }

        public Venue() { }
    }
}