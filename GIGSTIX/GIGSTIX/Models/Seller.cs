﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Seller : User
    {
        public List<string> EventsNames { get; set; } = new List<string>();
        public List<Event> Events { get; set; } = new List<Event>();
        public List<int> MyReservations { get; set; } = new List<int>();

        public Seller() { }

        public Seller(string[] parts) : base(parts)
        {
            for (int i = 8; i < parts.Length; i++)
            {
                EventsNames.Add(parts[i]);
            }
        }

        public override string ToString()
        {
            if (Events.Count > 0)
            {
                var eventsString = "";
                const char sep = ';';
                foreach (var e in Events)
                {
                    if (string.IsNullOrEmpty(eventsString))
                        eventsString = eventsString + e.Name;
                    else
                        eventsString = eventsString + sep + e.Name;
                }

                return $"{Username};{Password};{Name};{LastName};{GetGenderString(Gender)};{Birthday.ToString("dd-MM-yyyy")};{GetRoleString(Role)};{IsDeleted.ToString()};{eventsString}";
            }
            else
                return $"{Username};{Password};{Name};{LastName};{GetGenderString(Gender)};{Birthday.ToString("dd-MM-yyyy")};{GetRoleString(Role)};{IsDeleted.ToString()}";
        }

        public override void AppendToFile(string fileName)
        {
            var path = Constants.DATA_ROOT_PATH + fileName;
            File.AppendAllText(path, this.ToString() + Environment.NewLine);
        }

        public void RetrieveAllEvents()
        {
            foreach (string e in EventsNames)
            {
                Events.Add(AllTickets.GetEvent(e));
            }
        }

        public void RetrieveMyReservations()
        {
            foreach (Reservation r in Models.AllTickets.Reservations)
            {
                if (r.Seller.Username == Username)
                {
                    MyReservations.Add(r.Id);
                }
            }
        }
    }
}