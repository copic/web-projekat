﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Location
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Venue Venue { get; set; }

        public Location() { }

        public override string ToString()
        {
            //return Venue.Street + Venue.City +  Venue.PostNumber + Longitude + Latitude;
            return $"{Venue.Street}\n{Venue.City} {Venue.PostNumber}\n{Longitude}, {Latitude}";
        }
    }
}