﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Event Event { get; set; }
        public float Price { get; set; }
        public Buyer Buyer { get; set; }
        public TicketStatus Status { get; set; }
        public TicketType Type { get; set; }
        
        public Ticket() { }
    }
}