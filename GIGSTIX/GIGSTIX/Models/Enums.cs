﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }

    public enum Role
    {
        Admin,
        Seller,
        Buyer
    }
    
    public enum UserTypeEnum
    {
        Golden,
        Silver,
        Bronze
    }

    public enum Rating
    {
        One = 1,
        Two,
        Three,
        Four,
        Five
    }

    public enum TicketType
    {
        VIP,
        REGULAR,
        FAN_PIT
    }

    public enum TicketStatus
    {
        Reserved,
        Canceled
    }

    public enum ReservationStatus
    {
        Pending,
        Confirmed
    }
}