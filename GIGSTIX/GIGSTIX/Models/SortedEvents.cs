﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public static class SortedEvents
    {
        public static List<Event> Events { get; set; } = new List<Event>();

        public static void Update()
        {
            var _events = new List<Event>();

            foreach (Event e in AllTickets.Events)
            {
                _events.Add(e);
            }

            Events = _events.OrderBy(d => d.Date).ToList();
        }
    }
}