﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public Ticket Ticket { get; set; }
        public Buyer Buyer { get; set; }
        public Seller Seller { get; set; }
        public ReservationStatus Status {get;set;}

        public Reservation() { }
    }
}