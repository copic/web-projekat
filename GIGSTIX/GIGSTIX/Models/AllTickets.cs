﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace GIGSTIX.Models
{
    public static class AllTickets
    {
        public static List<Ticket> Tickets = new List<Ticket>();
        public static List<Event> Events = new List<Event>();
        public static List<Comment> Comments = new List<Comment>();
        public static List<Reservation> Reservations = new List<Reservation>();

        public static string ticketsPath = Constants.DATA_ROOT_PATH + "Tickets.xml";
        public static string eventsPath = Constants.DATA_ROOT_PATH + "Events.xml";
        public static string commentsPath = Constants.DATA_ROOT_PATH + "Comments.xml";
        public static string reservationsPath = Constants.DATA_ROOT_PATH + "Reservations.xml";

        public static void Serialize()
        {
            var serializer = new XmlSerializer(Tickets.GetType());

            using (var file = File.Create(ticketsPath))
            {
                using (var writer = XmlWriter.Create(file, new XmlWriterSettings { Indent = true }))
                {
                    serializer.Serialize(writer, Tickets);
                }
            }

            serializer = new XmlSerializer(Events.GetType());

            using (var file = File.Create(eventsPath))
            {
                using (var writer = XmlWriter.Create(file, new XmlWriterSettings { Indent = true }))
                {
                    serializer.Serialize(writer, Events);
                }
            }

            serializer = new XmlSerializer(Comments.GetType());

            using (var file = File.Create(commentsPath))
            {
                using (var writer = XmlWriter.Create(file, new XmlWriterSettings { Indent = true }))
                {
                    serializer.Serialize(writer, Comments);
                }
            }

            serializer = new XmlSerializer(Reservations.GetType());

            using(var file = File.Create(reservationsPath))
            {
                using (var writer = XmlWriter.Create(file, new XmlWriterSettings { Indent = true }))
                {
                    serializer.Serialize(writer, Reservations);
                }
            }
        }

        public static void Deserialize()
        {
            var serializer = new XmlSerializer(Tickets.GetType());

            using(var reader = XmlReader.Create(ticketsPath))
            {
                Tickets = (List<Ticket>)serializer.Deserialize(reader);
            }

            serializer = new XmlSerializer(Events.GetType());

            using(var reader = XmlReader.Create(eventsPath))
            {
                Events = (List<Event>)serializer.Deserialize(reader);
            }

            serializer = new XmlSerializer(Comments.GetType());

            using(var reader = XmlReader.Create(commentsPath))
            {
                Comments = (List<Comment>)serializer.Deserialize(reader);
            }

            serializer = new XmlSerializer(Reservations.GetType());

            using(var reader = XmlReader.Create(reservationsPath))
            {
                Reservations = (List<Reservation>)serializer.Deserialize(reader);
            }
        }

        public static Ticket GetTicket(int id)
        {
            foreach(Ticket t in Tickets)
            {
                if (t.Id == id)
                    return t;
            }

            return null;
        }

        public static Event GetEvent(string name)
        {
            foreach(Event e in Events)
            {
                if (e.Name == name)
                    return e;
            }

            return null;
        }

        public static Comment GetComment(int id)
        {
            foreach(Comment c in Comments)
            {
                if (c.Id == id)
                    return c;
            }

            return null;
        }

        public static Reservation GetReservation(int id)
        {
            foreach(Reservation r in Reservations)
            {
                if (r.Id == id)
                    return r;
            }

            return null;
        }
    }
}