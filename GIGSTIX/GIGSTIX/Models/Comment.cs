﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GIGSTIX.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public Rating Rating { get; set; }
        public Event Event { get; set; }
        public Buyer Buyer { get; set; }
        
        public Comment() { }

        public static Rating GetRating(string rating)
        {
            switch (rating.ToLower())
            {
                case "five":
                    return Rating.Five;
                case "four":
                    return Rating.Four;
                case "three":
                    return Rating.Three;
                case "two":
                    return Rating.Two;
                default:
                    return Rating.One;
            }
        }
    }
}