﻿using GIGSTIX.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace GIGSTIX.Controllers
{
    public class GIGSTIXController : Controller
    {
        // GET: GIGSTIX
        public ActionResult Index()
        {
            #pragma warning disable IDE0019
            User user = Session["user"] as User;
            #pragma warning restore IDE0019

            if (user == null)
            {
                user = new User();
                Session["user"] = user;
                return View();
            }

            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            foreach (var user in RegisteredUsers.AllUsers)
            {
                if (user.Username != username)
                    continue;
                if (user.Password != password)
                    return View("PasswordError");

                user.IsLogIn = true;
                Session["user"] = user;

                switch (user.Role)
                {
                    case Role.Admin:
                        return View("AdminView", user);
                    case Role.Seller:
                        return View("SellerView", user);
                    case Role.Buyer:
                        return View("BuyerView", user);
                    default:
                        return View("Error");
                }
            }

            return View("Error");
        }

        [HttpPost]
        public ActionResult Registration(string username, string password, string name, string lastname, string gender, string birthday)
        {
            var genderEnum = Models.User.GetGender(gender);

            var buyer = new Buyer()
            {
                Username = username,
                Password = password,
                Name = name,
                LastName = lastname,
                Gender = genderEnum,
                Birthday = DateTime.ParseExact(birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                Role = Models.Role.Buyer,
                Points = 0,
                UserType = new UserType
                {
                    Discount = 0,
                    Points = 10,
                    UserTypeEnum = UserTypeEnum.Bronze
                },
                IsDeleted = false,
                IsLogIn = false
            };

            foreach (var b in RegisteredUsers.Buyers)
                if (b.Username == username)
                    return View("UsernameTaken");

            RegisteredUsers.AllUsers.Add(buyer);
            RegisteredUsers.Buyers.Add(buyer);

            buyer.AppendToFile("Users.txt");

            return View("Index");
        }

        public ActionResult SignOut()
        {
            Session.Abandon();
            return View("Index");
        }

        public ActionResult AddSeller()
        {
            return View();
        }

        public ActionResult AddSellerImplementation(string username, string password, string name, string lastname, string gender, string birthday)
        {
            var genderEnum = Models.User.GetGender(gender);

            var seller = new Seller()
            {
                Username = username,
                Password = password,
                Name = name,
                LastName = lastname,
                Gender = genderEnum,
                Birthday = DateTime.ParseExact(birthday, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                Role = Models.Role.Seller,
                Events = new List<Event>(),
                IsLogIn = false
            };

            foreach (var s in RegisteredUsers.Sellers)
                if (s.Username == username)
                    return View("UsernameTaken");

            RegisteredUsers.AllUsers.Add(seller);
            RegisteredUsers.Sellers.Add(seller);

            seller.AppendToFile("Users.txt");

            var admin = Session["user"];
            return View("Success");
        }

        public ActionResult BackToProfile()
        {
            var user = Session["user"] as User;

            switch (user.Role)
            {
                case Role.Admin:
                    return View("AdminView", user as Admin);
                case Role.Seller:
                    return View("SellerView", user as Seller);
                case Role.Buyer:
                    return View("BuyerView", user as Buyer);
                default:
                    throw new Exception("Unreachable");
            }
        }

        public ActionResult DeleteUser(string username)
        {
            foreach (var u in RegisteredUsers.AllUsers)
            {
                if (u.Username != username || u.IsDeleted)
                    continue;

                u.IsDeleted = true;

                RegisteredUsers.UpdateUsers();

                var user = Session["user"] as User;
                return View("Success", user);
            }

            return View("Error");
        }

        public ActionResult EditProfile()
        {
            var user = Session["user"] as User;

            return View(user);
        }

        public ActionResult EditProfileImplementation(string username, string password, string name, string lastname, string gender, string birthday)
        {
            User user = null;

            foreach (var u in RegisteredUsers.AllUsers)
            {
                if (u.Username == username)
                    continue;

                u.Name = name;
                u.Password = password;
                u.LastName = lastname;
                u.Gender = Models.User.GetGender(gender);

                if(!string.IsNullOrEmpty(birthday))
                    u.Birthday = DateTime.Parse(birthday);

                user = u;
                break;
            }

            // Deleting old file and creating a new(edited) one
            var filename = user.Role == Role.Admin ? "Admins.txt" : "Users.txt";
            System.IO.File.Delete(Constants.DATA_ROOT_PATH + filename);

            switch (user.Role)
            {
                case Role.Admin:
                    foreach (var temp in RegisteredUsers.Admins)
                        temp.AppendToFile(filename);
                    return View("AdminView", user as Admin);
                case Role.Buyer:
                    foreach (var temp in RegisteredUsers.Buyers)
                        temp.AppendToFile(filename);
                    return View("BuyerView", user as Buyer);
                case Role.Seller:
                    foreach (var temp in RegisteredUsers.Sellers)
                        temp.AppendToFile(filename);
                    return View("SellerView", user as Seller);
                default:
                    return View("Error");
            }
        }

        public ActionResult FilterEvents(string name, string location, string date, string priceFrom, string priceTo, string type, string unsold)
        {
            DateTime _date;
            List<Event> filter = null;
            List<Event> unsoldFilter = new List<Event>();

            if (!string.IsNullOrEmpty(date))
            {
                _date = DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                filter = (from e in SortedEvents.Events
                          where (e.IsName(name) && e.IsLocation(location) && e.IsDate(_date.ToShortDateString()) && e.IsPrice(priceFrom, priceTo) && e.IsType(type))
                          select e).ToList();
            }
            else
            {
                filter = (from e in SortedEvents.Events
                          where (e.IsName(name) && e.IsLocation(location) && e.IsDate(date) && e.IsPrice(priceFrom, priceTo) && e.IsType(type))
                          select e).ToList();
            }
            
            if(unsold == "unsold")
            {
                foreach (Event e in filter)
                    if (e.AvailableTickets > 0)
                        unsoldFilter.Add(e);

                return View("FilteredEvents", unsoldFilter);
            }

            return View("FilteredEvents", filter);
        }

        public ActionResult SortEvents(string sortBy, string order)
        {
            var events = new List<Event>();
            var sortedEvents = new List<Event>();

            foreach (var e in SortedEvents.Events)
                events.Add(e);

            if (sortBy == "name")
                sortedEvents = order == "ascending" ? events.OrderBy(e => e.Name).ToList() : 
                                                      events.OrderByDescending(e => e.Name).ToList();

            if (sortBy == "date")
                sortedEvents = order == "ascending" ? events.OrderBy(e => e.Date).ToList() :
                                                      events.OrderByDescending(e => e.Date).ToList();

            if (sortBy == "price")
                sortedEvents = order == "ascending" ? events.OrderBy(e => e.Price).ToList() :
                                                      events.OrderByDescending(e => e.Price).ToList();

            if (sortBy == "location")
                sortedEvents = order == "ascending" ? events.OrderBy(e => e.Location.Venue.City).ToList() :
                                                      events.OrderByDescending(e => e.Location.Venue.City).ToList();

            return View("SortedEvents", sortedEvents);
        }

        public ActionResult ShowEvent(string name)
        {
            Event e = AllTickets.GetEvent(name);
            return View("ShowEvent", e);
        }

        public ActionResult AddEvent()
        {
            var user = Session["user"] as User;
            return View(user);
        }

        public ActionResult AddEventImplementation(string name, string type, string availableTickets, string date, string price, string city, string street, string number, string postNumber, string image, string username)
        {
            Seller seller = RegisteredUsers.GetSeller(username);

            Event e = new Event()
            {
                Name = name,
                EventType = type,
                AvailableTickets = Int32.Parse(availableTickets),
                Date = DateTime.Parse(date),
                Price = float.Parse(price),
                Status = false,
                Image = image,
                Location = new Location()
                {
                    Latitude = 45.267136,
                    Longitude = 19.833549,
                    Venue = new Venue
                    {
                        City = city,
                        Street = street,
                        Number = number,
                        PostNumber = postNumber
                    }
                },
                AverageRating = 0,
                Seller = seller.Username
            };

            AllTickets.Events.Add(e);
            SortedEvents.Update();
            AllTickets.Serialize();
            seller.Events.Add(e);
            RegisteredUsers.UpdateUsers();

            var user = Session["user"] as User;
            return View("Success", user);
        }

        public ActionResult AproveEvent(string status, string name)
        {
            foreach (Event e in AllTickets.Events)
            {
                if (e.Name == name)
                {
                    e.Status = bool.Parse(status);

                    AllTickets.Serialize();

                    var user = Session["user"] as User;
                    return View("AdminView", user);
                }
            }

            return View("Error");
        }

        public ActionResult DeleteEvent(string name)
        {
            foreach(Event e in AllTickets.Events)
            {
                if(e.Name == name)
                {
                    e.IsDeleted = true;
                }
            }

            AllTickets.Serialize();
            SortedEvents.Update();

            var user = Session["user"] as User;
            return View("AdminView", user);
        }

        public ActionResult BuyTicket(string eventName, string buyerUsername, string sellerUsername)
        {
            Event _event = AllTickets.GetEvent(eventName);
            Buyer _buyer = RegisteredUsers.GetBuyer(buyerUsername);
            Seller _seller = RegisteredUsers.GetSeller(sellerUsername);

            Reservation reservation = new Reservation()
            {
                Id = AllTickets.Reservations.Count() + 1,
                Ticket = new Ticket()
                {
                    Event = _event,
                    Id = AllTickets.Tickets.Count() + 1,
                    Buyer = RegisteredUsers.GetBuyer(buyerUsername),
                    Date = DateTime.Now.Date,
                    Price = _event.Price,
                    Status = TicketStatus.Reserved,
                    Type = TicketType.REGULAR
                },

                Status = ReservationStatus.Pending,
                Buyer = RegisteredUsers.GetBuyer(buyerUsername),
                Seller = RegisteredUsers.GetSeller(sellerUsername)
            };

            AllTickets.Tickets.Add(reservation.Ticket);
            AllTickets.Reservations.Add(reservation);
            _seller.MyReservations.Add(reservation.Id);
            //_buyer.AllTickets.Add(reservation.Ticket);
            _buyer.MyReservations.Add(reservation.Id);
            AllTickets.Serialize();

            var user = Session["user"] as User;
            return View("Success", user);
        }

        public ActionResult AproveReservation()
        {

            return View("SellerView");
        }

        public ActionResult SortUsers(string sortBy, string order)
        {
            var users = new List<User>();
            var usersSorted = new List<User>();

            foreach (var u in RegisteredUsers.AllUsers)
                users.Add(u);

            if (sortBy == "name")
                usersSorted = order == "ascending" ? users.OrderBy(u => u.Name).ToList() :
                                                      users.OrderByDescending(u => u.Name).ToList();

            if (sortBy == "lastname")
                usersSorted = order == "ascending" ? users.OrderBy(u => u.LastName).ToList() :
                                                      users.OrderByDescending(u => u.LastName).ToList();
            if (sortBy == "username")
                usersSorted = order == "ascending" ? users.OrderBy(u => u.Username).ToList() :
                                                      users.OrderByDescending(u => u.Username).ToList();

            return View("SortedUsers", usersSorted);
        }

        public ActionResult FilterUsers(string name, string lastname, string username, string role)
        {
            List<User> filter = null;

            filter = (from u in RegisteredUsers.AllUsers
                      where (u.IsName(name) && u.IsLastName(lastname) && u.IsUsername(username) && u.IsRole(role))
                      select u).ToList();

            return View("FilteredUsers", filter);
        }

        public ActionResult AddComment(string eventName)
        {
            EventForComment.EventName = eventName;

            var user = Session["user"] as User;
            return View(user);
        }

        public ActionResult AddCommentImpl(string content, string rating, string buyerUsername)
        {
            Event e;

            Comment comment = new Comment()
            {
                Content = content,
                Rating = Comment.GetRating(rating),
                Buyer = RegisteredUsers.GetBuyer(buyerUsername),
                Event = AllTickets.GetEvent(EventForComment.EventName)
            };

            AllTickets.Comments.Add(comment);
            AllTickets.Serialize();

            e = AllTickets.GetEvent(EventForComment.EventName);
            e.Comments.Add(comment);

            var user = Session["user"] as User;

            return View("Success", user);
        }
    }
}