﻿using GIGSTIX.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GIGSTIX
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            
            LoadUsers();
            //XmlsInit();
            AllTickets.Deserialize();
            SortedEvents.Update();

            foreach(Seller s in RegisteredUsers.Sellers)
            {
                s.RetrieveAllEvents();
                s.RetrieveMyReservations();
            }

            foreach(Event e in AllTickets.Events)
            {
                e.RetrieveComments();
            }

            foreach(Buyer b in RegisteredUsers.Buyers)
            {
                b.RetrieveMyReservations();
            }
        }

        private void LoadUsers()
        {
            const char separator = ';';

            var usersPath = Constants.DATA_ROOT_PATH + "Users.txt";
            if (!File.Exists(usersPath))
                throw new Exception("Could not find file: " + usersPath);

            using(var reader = new StreamReader(usersPath))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    var parts = line.Split(separator);

                    var user = new User(parts);

                    switch (Models.User.GetRole(parts[6]))
                    {
                        case Role.Admin:
                            var admin = new Admin(parts);
                            RegisteredUsers.Admins.Add(admin);
                            RegisteredUsers.AllUsers.Add(admin);
                            break;
                        case Role.Seller:
                            var seller = new Seller(parts);
                            RegisteredUsers.Sellers.Add(seller);
                            RegisteredUsers.AllUsers.Add(seller);
                            break;
                        default:
                            var buyer = new Buyer(parts);
                            RegisteredUsers.Buyers.Add(buyer);
                            RegisteredUsers.AllUsers.Add(buyer);
                            break;
                    }
                }
            }

            var adminsPath = Constants.DATA_ROOT_PATH + "Admins.txt";
            if (!File.Exists(adminsPath))
                throw new Exception("Could not find file: " + adminsPath);

            using (var reader = new StreamReader(adminsPath))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    var parts = line.Split(separator);
                    var admin = new Admin(parts);
                    RegisteredUsers.Admins.Add(admin);
                    RegisteredUsers.AllUsers.Add(admin);
                }
            }
        }

        private void XmlsInit()
        {
            var _event = new Event()
            {
                Name = "Exit Festival",
                Image = "Image",
                AvailableTickets = 1000,
                Date = new DateTime(2020, 8, 13),
                EventType = "Concert",
                Price = 6000,
                Status = true,
                Location = new Location
                {
                    Latitude = 45.267136,
                    Longitude = 19.833549,
                    Venue = new Venue
                    {
                        City = "Novi Sad",
                        Street = "Petrovaradinska Tvrdjava",
                        Number = "1",
                        PostNumber = "21000"
                    }
                }
            };

            var _ticket = new Ticket()
            {
                Id = 1,
                Event = _event,
                Date = _event.Date,
                Buyer = RegisteredUsers.GetBuyer("copic"),
                Price = 6000,
                Status = TicketStatus.Reserved,
                Type = TicketType.REGULAR
            };

            var _comment = new Comment()
            {
                Buyer = RegisteredUsers.GetBuyer("copic"),
                Content = "All recommendations..",
                Event = _event,
                Rating = Rating.Five
            };

            AllTickets.Tickets.Add(_ticket);
            AllTickets.Events.Add(_event);
            AllTickets.Comments.Add(_comment);

            AllTickets.Serialize();
        }
    }
}
